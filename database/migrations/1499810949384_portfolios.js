'use strict'

const Schema = use('Schema')

class PortfoliosTableSchema extends Schema {
  up () {
    this.create('portfolios', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.integer('budget')
      table.string('img')
      table.integer('views')
      table.integer('visitors')
    })
  }
  down () {
    this.drop('portfolios')
  }
}

module.exports = PortfoliosTableSchema
