'use strict'

const Schema = use('Schema')

class OrdersTableSchema extends Schema {
  up () {
    this.create('orders', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.string('phone')
      table.string('service')
      table.integer('email')
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrdersTableSchema
