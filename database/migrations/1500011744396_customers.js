'use strict'

const Schema = use('Schema')

class CustomersTableSchema extends Schema {
  up () {
    this.create('customers', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.string('site')
      table.integer('phone')
      table.string('email')
      table.integer('budget')
      table.integer('support_price')
      table.boolean('deleted')
      table.boolean('active')
    })
  }
  down () {
    this.drop('customers')
  }
}

module.exports = CustomersTableSchema
