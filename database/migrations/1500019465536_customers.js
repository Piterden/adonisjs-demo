'use strict'

const Schema = use('Schema')

class CustomersTableSchema extends Schema {
  up () {
    this.table('customers', (table) => {
      table.date('payment_day')
    })
  }
  down () {
    this.table('customers', (table) => {
      // opposite of up goes here
    })
  }
}

module.exports = CustomersTableSchema
