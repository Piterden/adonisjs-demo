'use strict'

const Route = use('Route')

Route.group('authenticated', function () {
  Route.group('admin', function () {
    // Admin page
    Route.get('/', 'AdminController.index').as('admin')
    // File manager
    Route.get('/fm/', 'FileManagerController.fm')
    Route.get('/tm/', 'FileManagerController.tm')
    Route.post('/tm/', 'FileManagerController.edit')
    // Docs
    Route.get('/doc/', 'DocController.list')
    Route.on('/doc/add').render('pages.doc.add')
    Route.post('/doc/add', 'DocController.add')
    Route.post('/doc/delete/', 'DocController.delete')
    // Customers
    Route.get('/customer/', 'CustomerController.list')
    Route.on('/customer/add').render('pages.customer.add')
    Route.post('/customer/add', 'CustomerController.add')
    Route.post('/customer/delete/', 'CustomerController.delete')
    // Orders
    Route.get('/order/', 'OrderController.list')
    Route.on('/order/add').render('pages.order.add')
    Route.post('/order/add', 'OrderController.add')
    Route.post('/order/delete/', 'OrderController.delete')
    // Services
    Route.get('/service/', 'ServiceController.list')
    Route.on('/service/add').render('pages.service.add')
    Route.post('/service/add', 'ServiceController.add')
    Route.post('/service/delete/', 'ServiceController.delete')
    // Portfolio
    Route.get('/portfolio/', 'PortfolioController.list')
    Route.on('/portfolio/add').render('pages.portfolio.add')
    Route.post('/portfolio/add', 'PortfolioController.add')
    Route.post('/portfolio/delete/', 'PortfolioController.delete')
  }).prefix('/admin')
  Route.group('auth-private', function () {
    // Auth
    Route.get('/list', 'AuthController.list')
    Route.get('/logout', 'AuthController.logout')
  }).prefix('/auth')
}).middleware(['auth'])

Route.group('auth', function () {
  // Auth
  Route.on('login').render('pages.auth.login')
  Route.post('login', 'AuthController.login')
}).prefix('/auth')

// Site
Route.get('/', 'SiteController.index')
Route.post('/order', 'SiteController.order')

// Demo routes
Route.group('registration-demo', function () {
  // Auth
  Route.on('/add').render('pages.auth.add')
  Route.post('/add', 'RegisterController.doRegister')
}).prefix('/auth')
