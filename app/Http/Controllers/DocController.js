'use strict'

const Doc = use('App/Model/Doc')
const Validator = use('Validator')

class DocController {
  * add (request, response) {
    const form = request.only('title', 'url')
    const validation = yield Validator.validate(form, Doc.rules)

    if (validation.fails()) {
      yield request.withAll().andWith({errors: validation.messages()}).flash()
      response.redirect('back')
    } else {
      yield Doc.create(form)
      response.redirect('/admin/doc/')
    }
  }
  * delete (request, response) {
    const id = request.only('id').id
    const doc = yield Doc.query().where('id', id).first()

    if (doc) {
      yield doc.delete()
      yield request.withAll().andWith({success: 'Успешно удалена'}).flash()
    } else {
      yield request.withAll().andWith({
        errors: [{message: 'Не удалось удалить'}]
      }).flash()
    }

    response.redirect('back')
  }
  * list (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const docs = yield Doc.all()
      const data = {
        docs: docs.toJSON()
      }

      yield response.sendView('pages.doc.list', data)
    }
  }
}

module.exports = DocController
