'use strict'

const User = use('App/Model/User')

class AuthController {
  * index (request, response) {
    yield response.sendView('login')
  }

  * login (request, response) {
    const form = request.only('email', 'password')

    try {
      yield request.auth.attempt(form.email, form.password)
      response.redirect('/admin')
    } catch (e) {
      yield request.with({errors: [{message: e.message}]}).flash()
      response.redirect('back')
    }
  }

  * logout (request, response) {
    yield request.auth.logout()
    return response.redirect('/auth/login')
  }

  * list (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const users = yield User.all()
      const data = {
        users: users.toJSON()
      }

      yield response.sendView('pages.auth.list', data)
    }
  }
}

module.exports = AuthController
