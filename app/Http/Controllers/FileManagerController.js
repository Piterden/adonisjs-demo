'use strict'

const FS = use('Filesystem')
const filename = 'index.njk'

class FileManagerController {
  * fm (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const user = yield request.auth.getUser()
      const data = {
        username: user.username,
        email: user.email
      }
      yield response.sendView('pages.fm.fm', data)
    }
  }

  * tm (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const user = yield request.auth.getUser()
      const file = yield FS.connection('views').get(`site/${filename}`)
      const data = {
        username: user.username,
        email: user.email,
        filename: filename,
        file: file
      }

      yield response.sendView('pages.fm.tm', data)
    }
  }
  * edit (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const form = request.only('file')
      yield FS.connection('views').put(`site/${filename}`, form.file)
      yield response.json({ success: true })
    }
  }
}

module.exports = FileManagerController
