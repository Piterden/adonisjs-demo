'use strict'

const Service = use('App/Model/Service')
const Validator = use('Validator')

class ServiceController {
  * add (request, response) {
    const form = request.only('name', 'desc', 'price', 'support_price', 'text')
    const validation = yield Validator.validate(form, Service.rules)

    if (validation.fails()) {
      yield request.withAll().andWith({errors: validation.messages()}).flash()
      response.redirect('back')
    } else {
      yield Service.create(form)
      response.redirect('/admin/service')
    }
  }
  * delete (request, response) {
    const id = request.only('id').id
    const service = yield Service.query().where('id', id).first()

    if (service) {
      yield service.delete()
      yield request.withAll().andWith({success: 'Успешно удалена'}).flash()
    } else {
      yield request.withAll().andWith({
        errors: [{message: 'Не удалось удалить'}]
      }).flash()
    }
    response.redirect('back')
  }
  * list (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const services = yield Service.all()
      const data = {
        services: services.toJSON()
      }

      yield response.sendView('pages.service.list', data)
    }
  }
}

module.exports = ServiceController
