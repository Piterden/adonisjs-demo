'use strict'

const Site = exports = module.exports = {}
const Mail = use('Mail')
const Env = use('Env')

Site.sendOrderEmail = function * (data) {
  yield Mail.send('emails.order', data, message => {
    message.to(Env.get('MAIL_USERNAME'), 'DirectMAN.ru')
    message.from(Env.get('MAIL_USERNAME'))
    message.subject('Сообщение с сайта DirectMAN.ru')
  })
}
