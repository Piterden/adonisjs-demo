'use strict'

const Env = use('Env')
const Youch = use('youch')
const Http = exports = module.exports = {}

/**
 * handle errors occured during a Http request.
 *
 * @param  {Object} error
 * @param  {Object} request
 * @param  {Object} response
 */
Http.handleError = function * (error, request, response) {
  const status = error.status || 500

  /**
   * DEVELOPMENT REPORTER
   */
  if (Env.get('NODE_ENV') === 'development') {
    const youch = new Youch(error, request.request)
    const type = request.accepts('json', 'html')
    const formatMethod = type === 'json' ? 'toJSON' : 'toHTML'
    const formattedErrors = yield youch[formatMethod]()
    response.status(status).send(formattedErrors)
    return
  }

  if (error.status === 404) {
    yield response.status(404).sendView('errors.index', {code: 404, message: 'Страница не найдера'})
    return
  }

  if (error.status === 401 || error.name === 'InvalidLoginException') {
    yield response.status(401).sendView('errors.index', {code: 401, message: 'Доступ запрещен'})
    return
  }
  /**
   * PRODUCTION REPORTER
   */
  console.error(error)
  yield response.status(status).sendView('errors/index', {code: 777, message: 'Oooooooooooops, Неизвестная ошибка'})
}

/**
 * listener for Http.start event, emitted after
 * starting http server.
 */
Http.onStart = function () {
}
