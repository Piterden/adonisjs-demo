'use strict'

const Lucid = use('Lucid')

class Doc extends Lucid {
  static get rules () {
    return {
      title: 'required',
      url: 'required'
    }
  }
}

module.exports = Doc
