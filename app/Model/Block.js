'use strict'

const Lucid = use('Lucid')

class Block extends Lucid {

  static get rules () {
    return {
      title: 'required',
      section_id: 'required'
    }
  }

}

module.exports = Block
