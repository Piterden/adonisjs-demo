'use strict'

const Lucid = use('Lucid')

class Service extends Lucid {
  static get rules () {
    return {
      name: 'required',
      desc: 'required',
      price: 'required',
      support_price: 'required',
      text: 'required'
    }
  }
}

module.exports = Service
