'use strict'

const Lucid = use('Lucid')

class Order extends Lucid {
  static get rules () {
    return {
      name: 'required',
      phone: 'required',
      email: 'email'
    }
  }
}

module.exports = Order
