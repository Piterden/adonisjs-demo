'use strict'

const Lucid = use('Lucid')

class Portfolio extends Lucid {
  static get rules () {
    return {
      name: 'required',
      budget: 'required',
      views: 'required',
      visitors: 'required'
    }
  }
}

module.exports = Portfolio
