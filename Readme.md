# Adonis JS

## Install 

* `yarn install`
* `./ace migration:run`
* `yarn run serve:dev`

## How start?

* Go to `http://localhost/admin`
* Authorization ``login: demo@mail.ru password: demo``

## Where change config ?
` .env - config file for site`

## How registration new user ? 

* Go to `http://localhost/auth/add`
